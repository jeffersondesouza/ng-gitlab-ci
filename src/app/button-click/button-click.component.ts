import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button-click',
  templateUrl: './button-click.component.html',
  styleUrls: ['./button-click.component.css']
})
export class ButtonClickComponent implements OnInit {

  points = 1;

  constructor() { }

  ngOnInit() {
  }

  onIncrease(value) {
    this.points += value;
  }

}
