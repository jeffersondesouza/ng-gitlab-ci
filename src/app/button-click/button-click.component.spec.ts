import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonClickComponent } from './button-click.component';

describe('ButtonClickComponent', () => {
  let component: ButtonClickComponent;
  let fixture: ComponentFixture<ButtonClickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonClickComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonClickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('increase number when button pressed async', async(() => {
    expect(component.points).toBe(1);
    fixture.debugElement.nativeElement.querySelector('#button').click();
    expect(component.points).toBe(2);
  }));

  it('increase number when button pressed', () => {
    expect(component.points).toBe(1);
    fixture.debugElement.nativeElement.querySelector('#button').click();
    expect(component.points).toBe(2);
  });
});
